const mongoose = require('mongoose');
const config = require('./config');
const Category = require('./models/Category');
const Item = require('./models/Item');
const User = require('./models/User');
const { nanoid } = require('nanoid');

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);
    const collections = await mongoose.connection.db.listCollections().toArray();
    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }
    const [movieCat, fishingCat, laptopCat, carsCat] = await Category.create({
        title: 'movies',
        description: 'Movie dvds',

    }, {
        title: 'fishing',
        description: 'fishing wares'
    }, {
        title: 'laptops',
        description: "Laptop computers",
    }, {
        title: 'cars',
        description: "Automobiles"
    })
    const [Adyl,Muni,Batman] = await User.create({
        username:'Adyl',
        password:'123@ewqUU',
        displayedName:'Adylbek',
        phone:'222333444',
        token:nanoid()
    },{
        username:'Muni',
        password:'123@ewqUU',
        displayedName:'Munara',
        phone:'222333555',
        token:nanoid()
    },{
        username:'Batman',
        password:'iambatman',
        displayedName:'The Batman',
        phone:'222333666',
        token:nanoid()
    })


    await Item.create({
        title: "Men in Black",
        description: 'First part of Men in Black',
        price: 100,
        category: movieCat,
        image: 'fixtures/MenInBlack.jpg',
        owner: Adyl
    }, {
        title: 'Lenovo Legion 5 Gaming Laptop',
        description: 'Lenovo Legion 5 Gaming Laptop, 15.6" FHD (1920x1080) IPS Screen, AMD Ryzen 7 4800H Processor, 16GB DDR4, 512GB SSD, NVIDIA GTX 1660Ti, Windows 10, 82B1000AUS, Phantom Black',
        price: 7000,
        category: laptopCat,
        image: 'fixtures/legion5.jpg',
        owner: Muni
    }, {
        title: 'Tesla Roadster',
        description: 'С места до 96 километров в час Tesla Roadster разгоняется за 1,9 секунды, а до 160 километров в час — за 4,2 секунды.',
        price: 800000,
        category: carsCat,
        image: 'fixtures/roadster.jpg',
        owner:Batman
    }, {
        title: 'Berkley Cherrywood HD Spinning Fishing Rod',
        description: 'Hybrid design construction for un-matched sensitivity with the security of blank through handle construction',
        price: 200,
        category: fishingCat,
        image: 'fixtures/cherrywood.jpg',
        owner:Batman
    })
    // await User.create({
    //     username: 'user',
    //     password: "1qaz@WSX29",
    //     token: nanoid()

    // }, {
    //     username: 'admin',
    //     password: "1qaz@WSX29",
    //     token: nanoid()
    // })
    await mongoose.connection.close()
}

run().catch(console.error)