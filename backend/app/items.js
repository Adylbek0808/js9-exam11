const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const path = require('path')
const config = require('../config')
const userAuth = require('../middleware/userAuth');
const Item = require('../models/Item');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({ storage });
const router = express.Router();


router.get('/', async (req, res) => {
    try {
        const items = await Item.find();
        res.send(items)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.get('/:id', async (req, res) => {
    try {
        const item = await Item.findById(req.params.id).populate('owner', ['username','phone'])
        res.send(item)
    } catch (error) {
        res.status(400).send(error)
    }
})

router.post('/', [userAuth, upload.single('image')], async (req, res) => {
    try {
        const itemData = req.body;
        itemData.owner = req.user._id;
        if (req.file) {
            itemData.image = 'uploads/' + req.file.filename
        }
        const item = new Item(itemData);
        await item.save();
        return res.send(item)
    } catch (error) {
        return res.status(400).send(error)
    }
})



module.exports = router