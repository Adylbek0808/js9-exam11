import React from 'react';
import { Card, CardContent, CardMedia, Typography, makeStyles, CardActions, Button, Grid } from '@material-ui/core';
import { apiURL } from '../../config';
import { useSelector } from 'react-redux';

const useStyles = makeStyles(theme => ({
    root: {
        width: 700,
    },
    media: {
        height: 400,
    },
    description: {
        fontSize: '20px',
        margin: theme.spacing(2, 0)
    }
}));

const ItemDetailCard = ({ title, id, description, price, image, owner }) => {
    const classes = useStyles();
    const imagePath = apiURL + '/' + image
    const user = useSelector(state => state.users.user)


    return (
        <>
            <Grid container justify="center">
                <Card className={classes.root}>
                    <CardMedia
                        className={classes.media}
                        image={imagePath}
                        title={title}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="textPrimary" component="p" className={classes.description}>

                            {description}
                        </Typography>
                        <Typography>Posted by {owner.username} </Typography>
                        <Typography>phone:{owner.phone}</Typography>

                        <Typography variant="body2" color="textSecondary" component="p">
                            Price: {price} KGS
                        </Typography>

                    </CardContent>
                    <CardActions>
                        {user ? (
                            <Button color="primary">
                                Delete Item
                            </Button>
                        ) : (
                            <>
                            </>
                        )}

                    </CardActions>
                </Card>
            </Grid>


        </>
    );
};

export default ItemDetailCard;