import { Button, Grid, makeStyles} from '@material-ui/core';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import FileInput from '../UI/Form/FileInput';
import FormElement from '../UI/Form/FormElement';


const useStyles = makeStyles(theme => ({
    formStyle: {
        margin: theme.spacing(4, 0, 12),
        border: "1px solid blue",
        borderRadius: '10px',
        padding: theme.spacing(2)
    }
}));

const NewItemForm = ({ onSubmit, categories }) => {
    const classes = useStyles()
    const error = useSelector(state => state.items.itemsError)


    const [itemInfo, setItemInfo] = useState({
        title: '',
        description: '',
        image: '',
        category: '',
        price: ''
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setItemInfo(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setItemInfo(prevState => ({
            ...prevState,
            [name]: file
        }))
    }
    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(itemInfo).forEach(key => {
            formData.append(key, itemInfo[key]);
        });
        onSubmit(formData);
    };
    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message
        } catch (e) {
            return undefined
        }
    }
    return (
        <form className={classes.formStyle} onSubmit={submitFormHandler}>
            <Grid item container direction="column" spacing={2}>
                <FormElement
                    type="text"
                    label="Item title"
                    name='title'
                    value={itemInfo.title}
                    onChange={inputChangeHandler}
                    error={getFieldError('title')}

                />
                <FormElement
                    type="text"
                    label="Item description"
                    name='description'
                    value={itemInfo.description}
                    onChange={inputChangeHandler}
                    multiline
                    rows={3}
                    error={getFieldError('description')}
                />

                <Grid item xs>
                    <FileInput
                        name="image"
                        label="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>
                <Grid item xs>
                    <FormElement
                        label="Category"
                        name='category'
                        value={itemInfo.category}
                        onChange={inputChangeHandler}
                        select
                        categories={categories}
                        error={getFieldError('category')}
                    />

                </Grid>
                <FormElement
                    type="number"
                    label="Price"
                    name='price'
                    value={itemInfo.price}
                    onChange={inputChangeHandler}
                    error={getFieldError('price')}

                />

                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Send</Button>
                </Grid>

            </Grid>
        </form>
    );
};

export default NewItemForm;