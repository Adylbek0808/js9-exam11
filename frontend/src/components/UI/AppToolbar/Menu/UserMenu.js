import React from 'react';
import { useDispatch } from 'react-redux';
import { Button, Grid, Typography } from '@material-ui/core';
import { logoutUser } from '../../../../store/actions/usersActions';
import  {Link} from 'react-router-dom'


const UserMenu = ({ user }) => {
    const dispatch = useDispatch();

    return (
        <>
            <Grid container alignItems='center'>
                <Typography
                    color="inherit"
                    display='inline'
                >
                    Hello, {user.displayedName}!
                </Typography>
                <Button color='inherit' component={Link } to="/new_item" >Add a new Item</Button>
                <span>or</span>
                <Button color='inherit' onClick={() => dispatch(logoutUser())}>Logout</Button>
            </Grid>
        </>
    );
};

export default UserMenu;