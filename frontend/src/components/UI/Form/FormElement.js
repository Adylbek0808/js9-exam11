import React from 'react';
import PropTypes from 'prop-types';
import { Grid, MenuItem, TextField } from '@material-ui/core';



const FormElement = ({ required, name, label, value, onChange, error, type, autoComplete, multiline, rows, select, categories }) => {
    return (
        <Grid item xs>
            {select ? (
                <TextField
                    select
                    required={required}
                    type={type}
                    label={label}
                    name={name}
                    autoComplete={autoComplete}
                    value={value}
                    error={Boolean(error)}
                    helperText={error}
                    onChange={onChange}
                    multiline={multiline}
                    rows={rows}
                >
                    <MenuItem><i>Select a category</i></MenuItem>
                    {categories.map(category => (
                        <MenuItem
                            key={category._id}
                            value={category._id}
                        >
                            {category.title}
                        </MenuItem>
                    ))}
                </TextField>
            ) : (

                <TextField
                    required={required}
                    type={type}
                    label={label}
                    name={name}
                    autoComplete={autoComplete}
                    value={value}
                    error={Boolean(error)}
                    helperText={error}
                    onChange={onChange}
                    multiline={multiline}
                    rows={rows}
                />
            )}
        </Grid>
    );
};

FormElement.propTypes = {
    select: PropTypes.any,
    required: PropTypes.string,
    type: PropTypes.string,
    multiline: PropTypes.any,
    rows: PropTypes.any,
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    autoComplete: PropTypes.string,
    error: PropTypes.string,
    categories: PropTypes.any

}

export default FormElement;