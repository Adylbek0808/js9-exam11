import { Container, CssBaseline } from '@material-ui/core';
import { Route, Switch } from 'react-router-dom';

import AppToolbar from './components/UI/AppToolbar/AppToolbar';
import Register from './containers/Register/Register';
import Login from './containers/Login/Login'
import NewItem from './containers/NewItem/NewItem';
import ItemsScreen from './containers/ItemsScreen/ItemsScreen';
import ItemDetail from './containers/ItemDetail/ItemDetail';

const App = () => {
  return (
    <>
      <CssBaseline />
      <header>
        <AppToolbar />
      </header>
      <main>
        <Container maxWidth='xl'>
          <Switch>
            <Route path='/' exact component={ItemsScreen} />
            <Route path='/register' exact component={Register} />
            <Route path='/login' component={Login} />
            <Route path='/new_item' component={NewItem} />
            <Route path='/item_detail' component={ItemDetail} />
          </Switch>
        </Container>
      </main>
    </>
  );
}

export default App;
