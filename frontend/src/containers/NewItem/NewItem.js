import { Grid, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import NewItemForm from '../../components/NewItemForm/NewItemForm';
import { fetchCategories } from '../../store/actions/categoriesActions';
import { fetchItems, sendItem } from '../../store/actions/itemsActions';



const NewItem = () => {
    const dispatch = useDispatch()
    const categories = useSelector(state => state.categories.categories)

    useEffect(() => {
        dispatch(fetchCategories());
        dispatch(fetchItems())
    }, [dispatch])

    const onNewItemFormSubmit = async (itemData) => {
        await dispatch(sendItem(itemData))
    }

    return (

        <Grid item container direction="column" spacing={2}>
            <Grid item xs>
                <Typography variant="h6">Create new Item</Typography>
            </Grid>
            <Grid item xs>
                <NewItemForm categories={categories} onSubmit={onNewItemFormSubmit} />
            </Grid>

        </Grid>

    );
};

export default NewItem;