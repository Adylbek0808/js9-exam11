import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchItemDetail } from '../../store/actions/itemsActions';
import ItemDetailCard from '../../components/ItemDetailCard/ItemDetailCard';



const ItemDetail = (props) => {
    const dispatch = useDispatch()
    const itemID = (props.location.pathname).substr(13)
    const item = useSelector(state => state.items.itemDetail)
    const itemOwner = useSelector(state => state.items.itemDetailOwner)
    const loading = useSelector(state => state.items.itemsLoading)

    useEffect(() => {
        dispatch(fetchItemDetail(itemID))
    }, [dispatch, itemID])

    return (
        <>
            {loading ? (
                <div>Loading</div>
            ) : (
                <ItemDetailCard
                    id={item._id}
                    title={item.title}
                    image={item.image}
                    description={item.description}
                    price={item.price}
                    owner={itemOwner}
                />
            )}



        </>
    );
};

export default ItemDetail;