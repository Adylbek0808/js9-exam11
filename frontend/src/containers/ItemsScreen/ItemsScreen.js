import { CircularProgress, Grid, makeStyles, Typography } from '@material-ui/core';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { fetchItems } from '../../store/actions/itemsActions';
import SingleItemSmall from './SingleItemSmall';

const useStyles = makeStyles(theme => ({
    progress: {
        height: 200
    }
}));

const ItemsScreen = () => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const items = useSelector(state => state.items.items);
    const loading = useSelector(state => state.items.itemsLoading)

    useEffect(() => {
        dispatch(fetchItems())
    }, [dispatch])

    return (
        <>
            <Grid container direction="column" spacing={2}>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">Items</Typography>
                    </Grid>
                </Grid>
                <Grid item container spacing={1}>
                    {loading ? (
                        <Grid container justify="center" alignItems="center" className={classes.progress}>
                            <Grid item>
                                <CircularProgress />
                            </Grid>
                        </Grid>
                    ) : (
                        items.map(item => (
                            <SingleItemSmall
                                key={item._id}
                                id={item._id}
                                title={item.title}
                                price={item.price}
                                image={item.image}
                            />
                        ))
                    )}
                </Grid>
            </Grid>

        </>
    );

};

export default ItemsScreen;