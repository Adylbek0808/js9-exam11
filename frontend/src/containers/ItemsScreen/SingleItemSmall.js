import React from 'react';
import { Card, CardActionArea, CardContent, CardHeader, CardMedia, Grid, makeStyles } from '@material-ui/core'
import { apiURL } from '../../config';
import { Link } from 'react-router-dom';


const useStyles = makeStyles(theme => ({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
}))

const SingleItemSmall = ({ id, image, title, price }) => {
    const classes = useStyles();
    const imagePath = apiURL + '/' + image

    return (

        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardActionArea component={Link} to={'/item_detail/' + id}>
                    <CardHeader title={title} />
                    <CardMedia
                        image={imagePath}
                        title={title}
                        className={classes.media}
                    />
                    <CardContent>
                        <strong style={{ marginLeft: '10px' }}>
                            Price:{price} KGS
                    </strong>
                    </CardContent>
                </CardActionArea>

            </Card>
        </Grid>
    );
};

export default SingleItemSmall;