import { NotificationManager } from 'react-notifications';
import axiosApi from '../../axiosApi';
import { historyPush } from './historyActions';

export const FETCH_ITEMS_REQUEST = "FETCH_ITEMS_REQUEST";
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_ITEMS_FAILURE = "FETCH_ITEMS_FAILURE";

export const SEND_ITEM_SUCCESS = 'SEND_ITEM_SUCCESS';
export const SEND_ITEM_FAILURE = "SEND_ITEM_FAILURE";

export const FETCH_ITEM_DETAIL_SUCCESS = 'FETCH_ITEM_DETAIL_SUCCESS';
export const FETCH_ITEM_DETAIL_FAILURE = 'FETCH_ITEM_DETAIL_FAILURE';

const fetchItemsRequest = () => ({ type: FETCH_ITEMS_REQUEST });
const fetchItemsSuccess = items => ({ type: FETCH_ITEMS_SUCCESS, items });
const fetchItemsFailure = error => ({ type: FETCH_ITEMS_FAILURE, error });

const sendItemSuccess = () => ({ type: SEND_ITEM_SUCCESS });
const sendItemFailure = error => ({ type: SEND_ITEM_FAILURE, error })

const fetchItemDetailSuccess = item => ({ type: FETCH_ITEM_DETAIL_SUCCESS, item });
const fetchItemDetailFailure = error => ({ type: FETCH_ITEM_DETAIL_FAILURE, error })

export const fetchItems = () => {
    return async dispatch => {
        try {
            dispatch(fetchItemsRequest())
            const response = await axiosApi.get('/items')
            dispatch(fetchItemsSuccess(response.data))
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchItemsFailure(error.response.data))
            } else {
                dispatch(fetchItemsFailure({ global: 'No internet' }))
            }
        }
    }
}

export const fetchItemDetail = (id) => {
    return async dispatch => {
        try {
            dispatch(fetchItemsRequest())
            const response = await axiosApi.get('/items/' + id)
            dispatch(fetchItemDetailSuccess(response.data))
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(fetchItemDetailFailure(error.response.data))
            } else {
                dispatch(fetchItemDetailFailure({ global: 'No internet' }))
            }
        }
    }
}

export const sendItem = itemData => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = { 'Authorization': token }
            const response = await axiosApi.post('/items', itemData, { headers })
            dispatch(sendItemSuccess())
            dispatch(historyPush('/'))
            NotificationManager.success('Item successfully posted')
            console.log(response)
        } catch (error) {
            if (error.response && error.response.data) {
                dispatch(sendItemFailure(error.response.data))
            } else {
                dispatch(sendItemFailure({ global: "No internet" }))
            }

            console.log(error)
        }

    }
}