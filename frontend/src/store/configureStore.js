import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunkMiddleware from 'redux-thunk';
import { loadFromLocalStorage, saveToLocalStorage } from "./localStorage";

import usersReducer from "./reducers/usersReducer";
import categoriesReducer from "./reducers/categoriesReducer";
import itemsReducer from "./reducers/itemsReducer";



const rootReducer = combineReducers({
    users: usersReducer,
    categories: categoriesReducer,
    items: itemsReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunkMiddleware))
);

store.subscribe(() => {
    saveToLocalStorage({
        users: store.getState().users
    });
})

export default store