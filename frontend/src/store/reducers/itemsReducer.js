const { FETCH_ITEMS_REQUEST, FETCH_ITEMS_SUCCESS, FETCH_ITEMS_FAILURE, SEND_ITEM_FAILURE, FETCH_ITEM_DETAIL_SUCCESS, FETCH_ITEM_DETAIL_FAILURE } = require("../actions/itemsActions")

const initialState = {
    itemsLoading: false,
    items: [],
    itemSingle: null,
    itemsError: null,
    itemDetail: {},
    itemDetailOwner: {}
}

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ITEMS_REQUEST:
            return { ...state, itemsLoading: true }
        case FETCH_ITEMS_SUCCESS:
            return { ...state, itemsLoading: false, items: action.items }
        case FETCH_ITEMS_FAILURE:
            return { ...state, itemsLoading: false, itemsError: action.error }
        case SEND_ITEM_FAILURE:
            return { ...state, itemsError: action.error }
        case FETCH_ITEM_DETAIL_SUCCESS:
            return { ...state, itemsLoading: false, itemDetail: action.item, itemDetailOwner: action.item.owner }
        case FETCH_ITEM_DETAIL_FAILURE:
            return { ...state, itemsLoading: false, itemDetail: action.error }
        default:
            return state
    }
}

export default itemsReducer;